//
//  ResultData.swift
//  ClickApp
//
//  Created by Марат Маркосян on 01.02.2022.
//

import Foundation

struct ResultData: Decodable {
    let winner: String
    let loser: String
}

