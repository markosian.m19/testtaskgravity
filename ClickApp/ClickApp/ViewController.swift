//
//  ViewController.swift
//  ClickApp
//
//  Created by Марат Маркосян on 31.01.2022.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var startButton = UIButton()
    private lazy var backImage = UIImageView(image: UIImage(named: Constants.mainBackground))
    private lazy var timerLabel = UILabel()
    private lazy var aim = UIImageView(image: UIImage(named: Constants.aim))
    var result = ""
    var timer = Timer()
    var secondsPassed = Constants.zero
    var tapCounter = 0
    var resultManager = ResultWebView()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSubviews()
        setUpAutoLayout()
        addGuestureToAim()
    }

    private func setUpSubviews() {
        view.backgroundColor = .systemBackground
        resultManager.delegate = self
     
        view.addSubview(backImage)
        view.addSubview(startButton)
        view.addSubview(timerLabel)
        view.addSubview(aim)
        
        startButton.translatesAutoresizingMaskIntoConstraints = false
        backImage.translatesAutoresizingMaskIntoConstraints = false
        timerLabel.translatesAutoresizingMaskIntoConstraints = false
        
        startButton.setBackgroundImage(UIImage(systemName: "play.circle"), for: .normal)
        startButton.contentMode = .scaleAspectFit
        startButton.addTarget(self, action: #selector(showRulesVC), for: .touchUpInside)
        
        backImage.contentMode = .scaleAspectFill
        
        timerLabel.isHidden = true
        timerLabel.textColor = .blue
        
        aim.contentMode = .scaleAspectFit
        aim.isUserInteractionEnabled = true
        var maxX = view.safeAreaLayoutGuide.layoutFrame.maxX - aim.frame.width
        var maxY = view.safeAreaLayoutGuide.layoutFrame.maxY - aim.frame.height
        
        maxY = -maxY
        maxX = -maxX
        let xCoord = CGFloat.random(in: 0...maxX)
        let yCoord = CGFloat.random(in: 0...maxY)
        aim.frame = CGRect(x: Int(yCoord), y: Int(xCoord), width: 100, height: 100)
        aim.isHidden = true
        
        guard let sceneDelegate = view.window?.windowScene?.delegate as? SceneDelegate,
              let window = sceneDelegate.window else { return }
        window.rootViewController = self
        
        navigationController?.navigationBar.isHidden = true
    }

    private func setUpAutoLayout() {
        NSLayoutConstraint.activate([
            startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            startButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            startButton.heightAnchor.constraint(equalToConstant: Constants.oneHundred),
            startButton.widthAnchor.constraint(equalToConstant: Constants.oneHundred),
            
            backImage.topAnchor.constraint(equalTo: view.topAnchor),
            backImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            backImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            timerLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            timerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.ten),
            timerLabel.widthAnchor.constraint(equalToConstant: Constants.oneHundred),
            timerLabel.heightAnchor.constraint(equalToConstant: Constants.thirty)
            ])
    }
    
    private func addGuestureToAim() {
        let tapOnAim = UITapGestureRecognizer(target: self, action: #selector(changePosition))
        
        aim.addGestureRecognizer(tapOnAim)
    }
    
    @objc func changePosition() {
        tapCounter += 1
        print(tapCounter)
        var maxX = view.safeAreaLayoutGuide.layoutFrame.maxX - aim.frame.width
        var maxY = view.safeAreaLayoutGuide.layoutFrame.maxY - aim.frame.height
        maxY = -maxY
        maxX = -maxX
        let minX = view.frame.minX
        let minY = view.frame.minY
        let xCoord = CGFloat.random(in: maxX...minX)
        let yCoord = CGFloat.random(in: maxY...minY)
        aim.frame = CGRect(x: Int(-xCoord), y: Int(-yCoord), width: 100, height: 100)
    }
    
     @objc func showRulesVC() {
         let presentAlert = UIAlertController(title: Constants.rulesLabel, message: Constants.gameRules, preferredStyle: .alert)
         let action = UIAlertAction(title: Constants.gotIt, style: .cancel) { [self] (action) in
             timer.invalidate()
             secondsPassed = Constants.zero
             startButton.isHidden = true
             timerLabel.isHidden = false
             aim.isHidden = false

             
             timer = Timer.scheduledTimer(timeInterval: Constants.millisecond, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
             
             navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New game", style: .plain, target: self, action: #selector(startNewGame))
             navigationController?.navigationBar.isHidden = true
         }

         presentAlert.addAction(action)
         
         present(presentAlert, animated: true, completion: nil)
     }
    
    @objc func startNewGame() {
        timer.invalidate()
        timerLabel.isHidden = true
        startButton.isHidden = false
        aim.isHidden = true
        secondsPassed = Constants.zero
        tapCounter = 0
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc func updateTimer() {
        if secondsPassed < Constants.seven {
            secondsPassed += Constants.millisecond
            
            timerLabel.text = "Time: \(Double(round(Constants.thousand * secondsPassed)/Constants.thousand))"
        } else {
            timer.invalidate()
            timerLabel.isHidden = true
            navigationController?.navigationBar.isHidden = false
            if tapCounter >= 10 {
                aim.isHidden = true
                result = "winner"
                moveToWeb()
            } else {
                aim.isHidden = true
                result = "loser"
                moveToWeb()
            }
        }
    }
    
    private func moveToWeb() {
        resultManager.fetchResult()
        let rootVC = resultManager
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true, completion: nil)
    }
    
}

extension ViewController: ResultDelegate {
    func updateURL() -> String {
        return result
    }
    
    func didFailWithError(error: Error) {
        print("ERROR")
        print(error)
    }
    
}

