//
//  ResultWebView.swift
//  ClickApp
//
//  Created by Марат Маркосян on 01.02.2022.
//

import UIKit
import WebKit

protocol ResultDelegate {
    func updateURL() -> String
    func didFailWithError(error: Error)
}

class ResultWebView: UIViewController {
    
    private lazy var web = WKWebView()
    var delegate: ResultDelegate?
    
    override func loadView() {
        view = web
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpSubViews()
        fetchResult()
    }
    
    private func setUpSubViews() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backToGame))
        navigationController?.hidesBarsOnSwipe = true
        
    }
    
    func fetchResult() {
        let urlString = "https://2llctw8ia5.execute-api.us-west-1.amazonaws.com/prod"
        performRequest(with: urlString)
    }
     
    @objc func backToGame() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func performRequest(with urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { [self] data, response, error in
            if error != nil {
                delegate?.didFailWithError(error: error!)
                return
            }
            
            if let safeData = data {
                if let result = parseJSON(safeData) {
                        let myURL = URL(string: result.urlForWeb)
                        let myRequest = URLRequest(url: myURL!)
                        DispatchQueue.main.async {
                            web.load(myRequest)
                        }
                        
//                        let myURL = URL(string: "https://www.apple.com")
//                        let myRequest = URLRequest(url: myURL!)
//                        self.web.load(myRequest)

                }
            }
        }
        task.resume()
    }
    
    func parseJSON(_ resultData: Data) -> ResultModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(ResultData.self, from: resultData)
            let loserURL = decodedData.loser
            let winnerURL = decodedData.winner
            if delegate?.updateURL() == "winner" {
                let result = ResultModel(urlForWeb: winnerURL)
                DispatchQueue.main.async {
                    self.title = "You win!"
                }
                return result
            } else if delegate?.updateURL() == "loser" {
                let result = ResultModel(urlForWeb: loserURL)
                DispatchQueue.main.async {
                    self.title = "You lose"
                }
                return result
            }
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
        return nil
    }

}
